package com.tryforce.cliente;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.tryforce.cliente.model.Cliente;
import com.tryforce.cliente.model.Produto;
import com.tryforce.cliente.resources.ClienteImp;
import com.tryforce.cliente.resources.ProdutoImp;


public class Teste {
	
	public static void main(String[] args)throws Exception {
		
		
		//////////////////////////////////
		//			MENU PRODUTO
		//////////////////////////////////
		
		ProdutoImp produto = new ProdutoImp();
		Produto prod = new Produto();
		Scanner ler = new Scanner(System.in);
		Character opcao;
		Integer opcao1;
		
		
		do {
			System.out.println("DIGITE A OPCAO DESEJADA: ");
			System.out.println("INSERIR: 1");
			System.out.println("ALTERAR: 2");
			System.out.println("DELETAR: 3");
			System.out.println("CONSULTAR POR NOME: 4");
			System.out.println("CONSULTAR TODOS: 5");
			opcao1 = ler.nextInt();
			
			if(opcao1.equals(1)) {
				
				String descricao;
				
				System.out.println("Insira uma descrição: ");
				descricao = ler.next();
				
				produto.inserir(descricao);
				
			}
			else if(opcao1.equals(2)) {
				Integer meuId;
				String novaDescricao;
				
				produto.consultarTodos();
				
				System.out.println("Digite o ID que deseja alterar: ");
				meuId = ler.nextInt();
				
				System.out.println("Digite o novo Nome: ");
				novaDescricao = ler.next();
				
				produto.atualizar(meuId, novaDescricao);
				produto.consultarTodos();
			}
			else if(opcao1.equals(3)) {
				Integer meuId;
				
				produto.consultarTodos();
				System.out.println("Digite o ID que deseja excluir: ");
				meuId = ler.nextInt();
				
				produto.deletar(meuId);
				produto.consultarTodos();
			}
			else if(opcao1.equals(4)) {
				String consulta = null;
				
				System.out.println("Digite um nome para a busca: ");
				consulta = ler.next();
				
				produto.consultar(consulta);
			}
			else if(opcao1.equals(5)) {
				
				produto.consultarTodos();
				
			}
			else {
				System.out.println("OPCAO INV�LIDA");
			}
			System.out.println();
			System.out.println("DESEJA CONTINUAR S/N: ");
			opcao = ler.next().charAt(0);
			
		}while(opcao == 's' || opcao == 'S');
			
			throw new Exception("Sistema Ecerrado com Sucesso.");
		
		//////////////////////////////////
		//				MENU
		//////////////////////////////////
		
		
		
	}
}
