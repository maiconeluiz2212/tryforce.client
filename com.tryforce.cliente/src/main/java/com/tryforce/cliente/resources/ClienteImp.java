package com.tryforce.cliente.resources;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.tryforce.cliente.apoio.UnidadePersistencia;
import com.tryforce.cliente.model.Cliente;

public class ClienteImp {
	
	
	
	//////////////////////////////////
	//INSERIR
	/////////////////////////////////
	public void inserir(String nome, String cpf, String rg) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		
		try {
			em.getTransaction().begin();
	
			Cliente cliente1 = new Cliente();
			cliente1.setNome(nome);
			cliente1.setCpf(cpf);
			cliente1.setRg(rg);
			
			em.persist(cliente1);
			em.getTransaction().commit();
		
		} catch (Exception e) {
			
			e.printStackTrace();
			
			if(em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			
		} finally {
			em.close();
		}
	}
	
	//////////////////////////////////
	//UPDATE- ALTERAR COM PARAMETRO
	/////////////////////////////////
	public void atualizar(Integer idCliente, String novaDescricao) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
		em.getTransaction().begin();
		
		em.createQuery(" update "
					 + " from Cliente p "
					 + " set nome = :novaDescricao "
					 + " where id = :idCliente")
		.setParameter("novaDescricao", novaDescricao).setParameter("idCliente", idCliente)
		.executeUpdate();
		
		em.getTransaction().commit();
		
		} catch (Exception e) {
		
			e.printStackTrace();
		
			if(em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		
		} finally {
		
		em.close();
		
		}
	}
	
	//////////////////////////////////
	//UPDATE- DELETE COM PARAMETRO
	/////////////////////////////////
	public void deletar(Integer id) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
			em.getTransaction().begin();
			
			em.createQuery(" delete "
						 + " from Cliente p "
						 + " where id = :idCliente ")
			.setParameter("idCliente", id)
			.executeUpdate();
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			if(em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			
		} finally {
			
			em.close();
			
		}
	}
	
	//////////////////////////////////
	//CONSULTAR POR ALGUM PARAMETRO
	//////////////////////////////////
	public void consultar(String nomeCliente) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
			
			String jpql = " select c "
						+ " from Cliente c "
						+ " where c.nome = :nomeCliente";
			TypedQuery<Cliente> typedQuery = em.createQuery(jpql, Cliente.class).setParameter("nomeCliente", nomeCliente);
					
			List<Cliente> lista = typedQuery.getResultList();
			
			for(Cliente cliente: lista) {
				System.out.println(cliente.getId()+"-"+cliente.getNome()+"-"+cliente.getCpf()+"-"+cliente.getRg());
			}
			
		} catch (NoResultException e) {
			
			e.printStackTrace();
			
		}
	}
	
	//////////////////////////////////
	//CONSULTAR TODOS
	/////////////////////////////////
	public void consultarTodos() {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
			
			String jpql = " select c "
						+ " from Cliente c ";
			TypedQuery<Cliente> typedQuery = em.createQuery(jpql, Cliente.class);
					
			List<Cliente> lista = typedQuery.getResultList();
			
			for(Cliente cliente: lista) {
				System.out.println(cliente.getId()+"-"+cliente.getNome()+"-"+cliente.getCpf()+"-"+cliente.getRg());
			}
			
		} catch (NoResultException e) {
			
			e.printStackTrace();
			
		}
	}
}
