package com.tryforce.cliente.resources;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;


import com.tryforce.cliente.apoio.UnidadePersistencia;
import com.tryforce.cliente.model.LogClient;
import com.tryforce.cliente.model.Produto;

public class ProdutoImp {
	
	
	//////////////////////////////////
	//INSERIR
	/////////////////////////////////
	public void inserir(String descricao)throws Exception{
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
			em.getTransaction().begin();
	
			Produto produto = new Produto();
			LogClient log = new LogClient();
			produto.setDescricao(descricao);
			produto.setDataProduto(new Date());
			
			log.setDataLog(new Date());
			log.setMensagem("Produto salvo com sucesso");
			//log.setTipoLog(1);
			
			em.persist(produto);
			em.persist(log);
			
			em.getTransaction().commit();
		
		} catch (Exception e) {
			
			LogClient log = new LogClient();
			
			log.setDataLog(new Date());
			log.setMensagem("Produto salvo com sucesso");
			em.persist(log);
			em.getTransaction().commit();
			
			e.printStackTrace();
			em.close();
			
		} finally {
			em.close();
		}
	}
	
	//////////////////////////////////
	//UPDATE- ATUALIZAR COM PARAMETRO
	/////////////////////////////////
	public void atualizar(Integer idProduto, String novaDescricao) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
			em.getTransaction().begin();
			
			em.createQuery(" update "
						 + " from Produto p "
						 + " set descricao = :novaDescricao, "
						 + " where id = :idProduto")
			.setParameter("novaDescricao", novaDescricao)
			.setParameter("idProduto", idProduto)
			.executeUpdate();
		
		em.getTransaction().commit();
		
		} catch (Exception e) {
		
			e.printStackTrace();
		
			if(em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		
		} finally {
		
			em.close();
		
		}
	}
	
	//////////////////////////////////
	//UPDATE- DELETE COM PARAMETRO
	/////////////////////////////////
	public void deletar(Integer idProduto) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		try {
			em.getTransaction().begin();
			
			em.createQuery(" delete "
						 + " from Produto p "
						 + " where id = :idProduto ")
			.setParameter("idProduto", idProduto)
			.executeUpdate();
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			if(em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			
		} finally {
			
			em.close();
			
		}
	}
	
	//////////////////////////////////
	//CONSULTAR POR ALGUM PARAMETRO
	/////////////////////////////////
	public Produto consultar(String nomeProduto) {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		Produto produto = null ;
		
		try {
			
			Query q = em.createQuery(" select a "
								   + " from Produto a "
								   + " where a.descricao = :descricao ")
					.setParameter("descricao", nomeProduto);
			
					
			produto = (Produto) q.getSingleResult();
			
			System.out.println("ID: "+produto.getId()+" Descrição: ".concat(produto.getDescricao()));
			
				
		} catch (Exception e) {
			
			e.printStackTrace();
			
			em.close();
			
		} finally {
			
			em.close();
			
		}
		
		return produto;
	}
	
	//////////////////////////////////
	//CONSULTAR TODOS
	/////////////////////////////////
	public List<Produto> consultarTodos() {
		EntityManager em = UnidadePersistencia.createEntityManager();
		
		List<Produto> lista = null;
		
		try {
			
			em.getTransaction().begin();
			
			Query q = em.createQuery(" select a "
								   + " from Produto a ");
	
			 lista = q.getResultList();
		
			if(lista.size() == 0) {
				System.out.println("Nenhum Registro encontrado");
			}
			
			for(Produto produto: lista) {
				System.out.println(produto.getId()+"-".concat(produto.getDescricao())+"-"+produto.getDataProduto()); 
			}
		
		} catch (NoResultException e) {
		
			e.printStackTrace();
			em.close();
		
		} finally {
			
			em.close();
			
		}
		
		return lista;
		
	}

}
