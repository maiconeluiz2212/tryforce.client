package com.tryforce.cliente.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "PRODUTO")
@SequenceGenerator(name = "BoxSeq", sequenceName = "SEQ_BOX", allocationSize = 1)
@XmlRootElement(name = "boxJson")
public class Produto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "DESCRICAO", nullable = false)
	private String descricao;
	
	@Column(name = "DATA_PRODUTO")
	private Date dataProduto;
	
	@Column(name = "VALOR")
	private Double valor;
	
	@ManyToOne
	@JoinColumn(name = "ID_VENDA", nullable = true)
	private Venda venda;

	public Produto() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setIdVenda(Venda venda) {
		this.venda = venda;
	}

	public Date getDataProduto() {
		return dataProduto;
	}

	public void setDataProduto(Date dataProduto) {
		this.dataProduto = dataProduto;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
